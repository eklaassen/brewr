from django.contrib import admin
from brew_logs.models import BrewLog

# Register your models here.
class BrewLogAdmin(admin.ModelAdmin):
    pass

admin.site.register(BrewLog, BrewLogAdmin)