from django.apps import AppConfig


class BrewLogsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'brew_logs'
