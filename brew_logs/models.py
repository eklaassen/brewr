from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings

from recipes.models import Recipe
USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class BrewLog(models.Model):
    name = models.CharField(max_length=120)
    author = models.ForeignKey(
        USER_MODEL,
        related_name="brew_logs",
        on_delete=models.CASCADE,
        null=True,
    )
    recipe = models.ForeignKey(
        Recipe,
        related_name="brew_logs",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    batch_size = models.FloatField()
    style = models.CharField(max_length=25, blank=True)
    bitterness = models.FloatField(validators=[MaxValueValidator(250), MinValueValidator(0)], blank=True, null=True)
    color = models.FloatField(blank=True, null=True)
    abv = models.FloatField(validators=[MaxValueValidator(25), MinValueValidator(0)], blank=True, null=True)
    brewing_date = models.DateField()
    date_updated = models.DateTimeField(auto_now=True)
    secondary_date = models.DateField(blank=True, null=True)
    bottling_date = models.DateField(blank=True, null=True)
    brewing_notes = models.TextField(blank=True, null=True)
    hop_notes = models.TextField(blank=True, null=True)
    yeast_notes = models.TextField(blank=True, null=True)
    fermentation_notes = models.TextField(blank=True, null=True)
    packaging_notes = models.TextField(blank=True, null=True)
    appearance = models.CharField(max_length = 500, blank=True, null=True)
    aroma = models.CharField(max_length = 500, blank=True, null=True)
    flavor = models.CharField(max_length = 500, blank=True, null=True)
    mouthfeel = models.CharField(max_length = 500, blank=True, null=True)

    def __str__(self):
        return f"{self.name}, {self.batch_size}g {self.style} on {self.brewing_date}" 