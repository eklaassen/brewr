from django.urls import path

from brew_logs.views import (
    BrewLogCreateView,
    BrewLogDeleteView,
    BrewLogDetailView,
    BrewLogListView,
    BrewLogUpdateView,
)


urlpatterns = [
    path("", BrewLogListView.as_view(), name="list_brewlogs"),
    path("<int:pk>/", BrewLogDetailView.as_view(), name="show_brewlog"),
    path("create/", BrewLogCreateView.as_view(), name="create_brewlog"),
    path("<int:pk>/delete/", BrewLogDeleteView.as_view(), name="delete_brewlog"),
    path("<int:pk>/edit/", BrewLogUpdateView.as_view(), name="edit_brewlog"),
]