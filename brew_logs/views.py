from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from brew_logs.models import BrewLog


# Create your views here.
class BrewLogListView(LoginRequiredMixin, ListView):
    model = BrewLog
    template_name = "brew_logs/list.html"
    paginate_by = 20

    def get_queryset(self):
        return BrewLog.objects.filter(author=self.request.user)


class BrewLogDetailView(LoginRequiredMixin, DetailView):
    model = BrewLog
    template_name = "brew_logs/detail.html"


class BrewLogCreateView(LoginRequiredMixin, CreateView):
    model = BrewLog
    template_name = "brew_logs/create.html"
    fields = [
        "name", 
        "recipe", 
        "batch_size", 
        "style", 
        "bitterness", 
        "color", 
        "abv", 
        "brewing_date", 
        "secondary_date", 
        "bottling_date", 
        "brewing_notes",
        "hop_notes", 
        "yeast_notes", 
        "fermentation_notes", 
        "packaging_notes", 
        "appearance", 
        "flavor",
        "aroma", 
        "mouthfeel",
    ]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("show_brewlog", args=[self.object.id])


class BrewLogDeleteView(LoginRequiredMixin, DeleteView):
    model = BrewLog
    template_name = "brew_logs/delete.html"
    success_url = reverse_lazy("list_brewlogs")


class BrewLogUpdateView(LoginRequiredMixin, UpdateView):
    model = BrewLog
    template_name = "brew_logs/edit.html"
    fields = [
        "name", 
        "recipe", 
        "batch_size", 
        "style", 
        "bitterness", 
        "color", 
        "abv", 
        "secondary_date", 
        "bottling_date", 
        "brewing_notes", 
        "aroma", 
        "flavor",
        "hop_notes", 
        "yeast_notes", 
        "fermentation_notes", 
        "packaging_notes", 
        "appearance",
        "flavor",
        "aroma", 
        "mouthfeel"
    ]
    success_url = reverse_lazy("list_brewlogs")