from django.contrib import admin
from recipes.models import Recipe, Fermentable, Hop

# Register your models here.
class RecipeAdmin(admin.ModelAdmin):
    pass

class FermentableAdmin(admin.ModelAdmin):
    pass

class HopAdmin(admin.ModelAdmin):
    pass

admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Fermentable, FermentableAdmin)
admin.site.register(Hop, HopAdmin)