from django.db import models
from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
STYLE_CHOICES = (
    ("",""),
    ("lager","LAGER"),
    ("stout","STOUT"),
    ("sour","SOUR"),
)

FERMENTABLE_CHOICES = (
    ("",""),
    ("pilsner","Pilsner"),
    ("pale ale","Pale Ale"),
    ("white wheat","White Wheat"),
    ("caramel 60","Caramel / Crystal 60L"),
    ("chocolate","Chocolate"),
)

HOP_CHOCIES = (
    ("",""),
    ("cascade","Cascade"),
    ("citra","Citra"),
    ("mosaic","Mosaic"),
    ("simcoe","Simcoe"),
    ("fuggle","Fuggle"),
)

HOP_TYPES = (
    ("",""),
    ("pellet","Pellet"),
    ("whole cone","Whole Cone"),
)

class Recipe(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    image = models.URLField(null=True, blank=True)
    batch_size = models.FloatField(validators=[MinValueValidator(0)])
    style = models.CharField(max_length=5, choices=STYLE_CHOICES, default="")
    author = models.ForeignKey(
        USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Fermentable(models.Model):
    name = models.CharField(max_length=11, choices=FERMENTABLE_CHOICES, default="")
    amount = models.FloatField(validators=[MinValueValidator(0)])
    gravity = models.FloatField(validators=[MinValueValidator(0)])
    color = models.FloatField(validators=[MaxValueValidator(600), MinValueValidator(0)])
    recipe = models.ForeignKey(
        "Recipe",
        related_name="fermentables",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.amount}lbs - {self.name}"


class Hop(models.Model):
    variety = models.CharField(max_length=7, choices=HOP_CHOCIES, default="")
    amount = models.FloatField()
    hop_type = models.CharField(max_length=10, choices=HOP_TYPES, default="")
    alpha_acids = models.FloatField()
    time = models.TimeField(blank=True, null=True)
    recipe = models.ForeignKey(
        "Recipe",
        related_name="hops",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.amount}oz. {self.variety}"