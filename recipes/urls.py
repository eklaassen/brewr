from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeDeleteView,
    RecipeDetailView,
    RecipeListView,
    RecipeUpdateView,
)


urlpatterns = [
    path("", RecipeListView.as_view(), name="list_recipes"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="show_recipe"),
    path("create/", RecipeCreateView.as_view(), name="create_recipe"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="delete_recipe"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="edit_recipe"),
]