from django.urls import path

from tags.views import (
    TagCreateView,
    TagDeleteView,
    TagDetailView,
    TagListView,
    TagUpdateView
)


urlpatterns = [
    path("", TagListView.as_view(), name="list_tags"),
    path("<int:pk>/", TagDetailView.as_view(), name="show_tag"),
    path("create/", TagCreateView.as_view(), name="create_tag"),
    path("<int:pk>/delete/", TagDeleteView.as_view(), name="delete_tag"),
    path("<int:pk>/edit/", TagUpdateView.as_view(), name="edit_tag"),
]